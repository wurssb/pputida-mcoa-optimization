import pathlib

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import cobra

WT_results_location = pathlib.Path("results/WT_predictions.xlsx")
model_location = pathlib.Path("data/models/iJN1462_fbc.xml")
plot_data_location = pathlib.Path("data/plot_data.h5")
output_location = pathlib.Path("results/computational_results.svg")

model = cobra.io.read_sbml_model(str(model_location))

# Numbers for filtering
reactions = pd.read_excel(WT_results_location, sheet_name="reactions - all")

final_targets = {
    # "accBD": ["PP_0559", "PP_1996"], # Not a knock-down target
    "fabD": (["PP_1913"], "model"),
    "fabV": (["PP_4635"], "model"),
    "fabA": (["PP_4174"], "model"),
    "fabF": (["PP_1916"], "model"),
    "fabB": (["PP_4175"], "model"),
    "sucAB": (["PP_4189", "PP_4188"], "manual"),
    "fabH": (["PP_4379"], "model"),
    "gntT": (["PP_3417"], "model"),
    "ilvH": (["PP_4679"], "model"),
    "cyoC": (["PP_0814"], "model"),
    "pycAB": (["PP_5347", "PP_5346"], "manual"),
    "asnB": (["PP_1750"], "model"),
    "leuA": (["PP_1025"], "model"),
    "lysC": (["PP_4473"], "model"),
    "nuoC": (["PP_4121"], "model"),
    "gnuK": (["PP_3416"], "model"),
    "cyoAB": (["PP_0812", "PP_0813"], "model"),
    "glnA": (["PP_5046"], "model"),
    "fabZ": (["PP_1602"], "model"),
    "glcB": (["PP_0356"], "model"),
    "nuoA": (["PP_4119"], "model"),
    "proB": (["PP_0691"], "model"),
    "icd": (["PP_4011"], "manual"),
    "sdhABCD": (["PP_4193", "PP_4191", "PP_4190", "PP_4192"], "model"),
    "sucCD": (["PP_4186", "PP_4185"], "manual"),
    "idh": (["PP_4012"], "model"),
    "ppc": (["PP_1505"], "manual"),
    "mdh": (["PP_0654"], "manual"),
    "gntZ": (["PP_4043"], "model"),
    "metK": (["PP_4967"], "model"),
    "hldE": (["PP_4934"], "model"),
    "fumC-II": (["PP_1755"], "manual"),
    "aroE": (["PP_0074"], "manual"),
    "fumC-I": (["PP_0944"], "manual"),
    "scpC": (["PP_0154"], "manual"),
    "ilvE": (["PP_3511"], "manual"),
    "mqoII": (["PP_1251"], "manual"),
    "maeB": (["PP_5085"], "manual"),
    "acnA": (["PP_2112"], "model"),
    "argA": (["PP_5185"], "model"),
    "tyrB": (["PP_1972"], "manual"),
    "gltA": (["PP_4194"], "model"),
    "glmM": (["PP_4716"], "model"),
    "purA": (["PP_4889"], "model"),
}

all_reactions = {"R_" + r.id for r in model.reactions} | {
    "naphthsyn",
    "EX_naphth_export",
}

# Sets of reactions with criteria valid
active = set(reactions.reaction.unique())
inactive = all_reactions - active
iso_enzyme = set(reactions.reaction[~reactions.iso_enzyme])
gene_annotated = set(reactions.reaction[(~reactions.gene_rule.isna())])
ks_growth_prod = set(
    reactions.reaction[
        ((reactions["ks-statistic (1v3)"] > 0.5) & (reactions["p-value (1v3)"] < 0.05))
    ]
)
ks_growth_slow = set(
    reactions.reaction[
        ((reactions["ks-statistic (2v3)"] > 0.25) & (reactions["p-value (2v3)"] < 0.05))
    ]
)
growth_correlation = set(reactions.reaction[(reactions.biomass_correlation < 0.9)])
mean_difference = set(reactions.reaction[(reactions.mean_absolute_change > 1e-2)])

all_criteria = set.intersection(
    iso_enzyme,
    gene_annotated,
    ks_growth_prod,
    ks_growth_slow,
    growth_correlation,
    mean_difference,
)
correlation_groups = len(
    reactions[reactions.reaction.isin(all_criteria)].correlation_group.unique()
)
groups = {
    "Model reactions": all_reactions,
    "Active": active,
    "Gene annotation": gene_annotated,
    "No iso-enzymes": iso_enzyme,
    "Difference growth\n and production": ks_growth_prod,
    "Difference production\nand slow growth": ks_growth_slow,
    "Biomass correlation": growth_correlation,
    "Absolute flux\ndifference": mean_difference,
}

accumulator = []
group_counts = {}
for name, group in groups.items():
    accumulator.append(group)
    group_counts[name] = len(set.intersection(*accumulator))

r = set.intersection(*accumulator)
g = set.union(*({g.id for g in model.reactions.get_by_id(rid[2:]).genes} for rid in r))

group_counts["Unique genes"] = len(g)
# final_targets_genes_model = set.union(
#     *(set(i) for i,j in final_targets.values() if j == "model")
# )
# final_targets_genes_manual = set.union(
#     *(set(i) for i, j in final_targets.values() if j == "manual")
# )
# Show targets instead of genes, since some genes are targeted as one in an operon.
group_counts["Targets selected\nthrough model"] = sum((j == 'model' for _, j in final_targets.values()))
group_counts["Manually selected\ntargets"] = sum((j == 'manual' for _, j in final_targets.values()))
group_counts["Final targets"] = len(final_targets)

group_df = (
    pd.Series(group_counts)
    .to_frame()
    .reset_index()
    .set_axis(["Criteria", "number"], axis=1)
)

df = pd.read_hdf(plot_data_location)
df = (
    df.set_index(list(df.columns[:3]))
    .stack()
    .reset_index()
    .rename(columns={"level_3": "reaction", 0: "flux"})
)
accoac = "ACCOAC (acc)"  # "Acetyl-CoA carboxylase (acc)"
mcoata = "MCOATA (fabD)"  # "ACP malonyltransferase (fabD)"
acc = "acc over-expression"
lys = "lysC knock-down"
tyr = "tyrB knock-down"
WT = "Wild type"


df = df.replace(
    {
        "WT": WT,
        "growing (pFBA)": "growing",
        "R_ACCOAC": accoac,
        "R_MCOATA": mcoata,
        "accBD": acc,
        "lysC": lys,
        "tyrB": tyr,
    }
)
plt.ion()

colors = sns.color_palette("tab10")

xlimits = {
    accoac: (0, 20),
    mcoata: (0, 3),
    acc: (0, 16),
    lys: (0, 3),
    tyr: (0, 3),
}
plotfunction = sns.histplot
plotargs = {
    sns.kdeplot: {"gridsize": 500, "cut": 0, "shade": True},
    sns.histplot: {"bins": 100, "common_bins": True, "stat": "probability"},
}
despineleft = False
flux_label = "Flux - {}\n"
flux_unit = r"$\mathrm{mmol}\ \mathrm{gDW}^{-1}\ \mathrm{h}^{-1}$"


# Figure set-up
fig = plt.figure(constrained_layout=True, figsize=(10, 7))
spec = fig.add_gridspec(nrows=6, ncols=3, width_ratios=[2, 2, 2])
ax_single_1 = fig.add_subplot(spec[2:4, 0])
ax_single_2 = fig.add_subplot(spec[4:, 0])
single_axes = [ax_single_1, ax_single_2]
ax_double_1 = fig.add_subplot(spec[:2, 1])
ax_double_2 = fig.add_subplot(spec[2:4, 1])
ax_double_3 = fig.add_subplot(spec[4:, 1])
double_axes = [ax_double_1, ax_double_2, ax_double_3]
ax_table = fig.add_subplot(spec[2:, 2])

# Single results
strain = WT
reactions = [accoac, mcoata]
hue_order = ["growing", "producing"]
palette = [colors[0], colors[2]]


for reaction, ax in zip(reactions, single_axes):
    subset = df[(df.strain == strain) & (df.reaction == reaction)]
    plotfunction(
        data=subset,
        x="flux",
        hue="condition",
        hue_order=hue_order,
        palette=palette,
        alpha=0.5,
        ax=ax,
        legend=False,
        **plotargs[plotfunction]
    )
    ax.set_xlabel(flux_label.format(subset.reaction.iloc[0]) + flux_unit)
    # ax.set_ylabel()
    if despineleft:
        ax.yaxis.set_visible(False)
    ax.set_xlim(*xlimits[reaction])
    sns.despine(ax=ax, left=despineleft)

reaction = mcoata
strains = [acc, lys, tyr]
palette = [colors[0], colors[2], colors[3], colors[1]]
for strain, ax in zip(strains, double_axes):
    hue_order = [
        "{} - growing".format(WT),
        "{} - producing".format(WT),
        "{} - growing".format(strain),
        "{} - producing".format(strain),
    ]
    subset = df[(df.strain.isin((WT, strain))) & (df.reaction == reaction)].copy()

    # Merge strain + condition
    subset.loc[:, "condition"] = subset.strain.str.cat(subset.condition, sep=" - ")
    plotfunction(
        data=subset,
        x="flux",
        hue="condition",
        hue_order=hue_order,
        palette=palette,
        alpha=0.5,
        ax=ax,
        legend=strain == strains[0],
        **plotargs[plotfunction]
    )
    if strain == strains[0]:
        labels = [
            hue_order[0],
            hue_order[1],
            "Mutant - growing".format(strain),
            "Mutant - producing".format(strain),
        ]
        lgd = ax.get_legend()
        handles = lgd.legendHandles
        lgd.remove()
        # Put the legend as a figure legend instead since it covers all 5 distribution plots.
        fig.legend(
            handles, labels, frameon=False, loc="upper right", bbox_to_anchor=(0.8, 1)
        )
    ax.set_xlabel(flux_label.format(subset.reaction.iloc[0]) + flux_unit)
    # ax.set_ylabel("Flux density")
    ax.set_title(strain, y=0.8, fontdict={"fontsize": "medium"})
    if despineleft:
        ax.yaxis.set_visible(False)
    ax.set_xlim(*xlimits[strain])
    sns.despine(ax=ax, left=despineleft)

sns.barplot(data=group_df, y="Criteria", x="number", ax=ax_table)
# Add numbers next to bars.
texts = []
for patch, number in zip(ax_table.patches, group_df.number):
    text = ax_table.text(
        patch.get_x() + patch.get_width() + 2,
        patch.get_y() + patch.get_height() / 2,
        str(number),
        ha="left",
        va="center",
    )
    texts.append(text)
ax_table.set_xlabel("")
ax_table.set_ylabel("")
ax_table.set_title("Target selection", fontdict={"fontsize": "medium"})
sns.despine(ax=ax_table)
fig.savefig(output_location)
