# Sampling tools

Analysis of targets for the over production of malonyl-CoA in *Pseudomonas Putida* through flux sampling.

### Usage:
The sampling code can be found in `sampling.py`. `example.py` contains the documented example applied to case of malonyl-CoA overproduction in *Pseudomonas putida*.
The figure for the results in the publication was generated using `results_plot.py`

The original iJN1462 can be found in `data/models/emi14843-sup-0003-supinfos3.xml`. Using `clean_model_iJN1462.py`, a few minor errors were corrected, which should not impact the final results. The updated model is saved in `data/models/iJN1462_fbc.xml`.

### Dependencies and installation:
The following dependencies are required to run the code. The versions listed in the parenthesis were verified to work on Ubuntu 20.04.
* `python` (3.7.9)
* `cobra` (0.19.0) using `gurobi` (9.0.3)
* `numpy` (1.19.1)
* `pandas` (1.1.4)
* `scipy` (1.5.2)
* `matplotlib` (3.3.1)
* `seaborn` (0.11.0)
* `python-libsbml` (5.18.1)
* `xlsxwriter` (1.3.6)
* `xlrd` (1.2.0)
* `openpyxl` (3.0.5)
* `pytables` (3.6.1)

If using conda, they can be automatically installed via the `enviroment.yml` file through `conda env create -f environment.yml`.

### License:
* MultiLevelPool (`multilevelpool.py`) is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), see the file header for details.
* The iJN1462 model (`data/models/emi14843-sup-0003-supinfos3.xml`) by Nogales *et al.* (2019) is licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/), and was retrieved from [doi: 10.1111/1462-2920.14843](https://dx.doi.org/10.1111/1462-2920.14843).
* Everything else is licensed under the MIT license (see `LICENSE`)
