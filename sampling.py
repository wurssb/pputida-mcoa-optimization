"""Utility functions for flux sampling."""
import itertools
import contextlib
import hashlib
import json
import os
import pathlib
import warnings

import cobra
import numpy as np
import pandas as pd
import scipy
import scipy.stats
import scipy.cluster

from multilevelpool import *

COBRA_SOFT_INFINITY = 1000


def sample(model, sampling_settings, cache_dir=pathlib.Path("cache"), name=None):
    # If the model is given as string, we try to load it using the json io.
    if isinstance(model, str):
        model = cobra.io.from_json(model)
    if cache_dir:
        hasher = hashlib.sha1()
        hasher.update(cobra.io.to_json(model, sort=True).encode("ascii"))
        # Drop the number of processes key since it doesn't change the result.
        # If we check the convergence on a per chain basis, the number of processes.
        # does matter, as it changes the individual chain length!
        # sampling_settings_copy = sampling_settings.copy()
        # sampling_settings_copy["processes"]
        hasher.update(json.dumps(sampling_settings, sort_keys=True).encode("ascii"))
        hash_str = hasher.hexdigest()
        cache_samples_path = cache_dir / (hash_str + "_samples.h5")
        cache_convergence_path = cache_dir / (hash_str + "_geweke.h5")
        name_str = " ({})".format(name) if name else ""
        if cache_samples_path.exists() and cache_convergence_path.exists():
            update_cache = False
            samples = convergence = None
            try:
                samples = pd.read_hdf(cache_samples_path)
                print(
                    "Samples retrieved from cache{}: {}".format(
                        name_str, str(cache_samples_path)
                    )
                )
            except Exception:
                warnings.warn(
                    "Could not load samples from cache{}: {}".format(
                        name_str, str(cache_samples_path)
                    )
                )
            try:
                convergence = pd.read_hdf(cache_convergence_path)
                print(
                    "Convergence scores retrieved from cache{}: {}".format(
                        name_str, str(cache_convergence_path)
                    )
                )
            except Exception:
                warnings.warn(
                    "Could not load convergence scores from cache{}: {}".format(
                        name_str, str(cache_convergence_path)
                    )
                )
            if samples is not None and convergence is not None:
                return samples, convergence
            else:
                update_cache = True
        else:
            warnings.warn(
                "Model and settings not found in cache{}: {}".format(
                    name_str, str(cache_samples_path)
                )
            )
            update_cache = True

    # TODO: Check convergence criteria (See 2019 paper)
    # Maybe we can use PyMC(3) functions?
    # Not sure if we have access to the individual chains?

    # Settings for sampler.
    n_samples = sampling_settings["n_samples"]
    optimality = sampling_settings["optimality"]
    thinning = sampling_settings["thinning"]
    processes = sampling_settings["processes"]
    objective_fraction = sampling_settings["objective_fraction"]
    flux_fraction = sampling_settings["flux_fraction"]
    cutoff_tolerance = sampling_settings["cutoff_tolerance"]
    biomass_reaction = model.reactions.get_by_id(sampling_settings["biomass_reaction"])
    target_export = model.reactions.get_by_id(sampling_settings["target_export"])

    # Check growth and production limits.
    b1, t1, b2, t2 = verify_target_production(
        model, biomass_reaction, target_export, target_export
    )

    # Check what growth would be still possible under the t2 * optimality constraint.
    # This will be the reference.
    with model:
        target_export.lower_bound = t2 * optimality
        model.objective = biomass_reaction
        slow_growth = model.optimize().objective_value

    # Runs we want to do.
    runs = [
        ("growing (pFBA)", biomass_reaction, "lower", b1 * optimality, True),
        ("growing (pFBA, slow)", biomass_reaction, "upper", slow_growth, True),
        ("producing", target_export, "lower", t2 * optimality, False),
    ]

    all_samples = []
    validity = []
    geweke_diagnostics = np.full((len(runs) * processes, len(model.reactions)), np.NaN)
    for run_index, (name, target, bound_direction, bound, add_pfba) in enumerate(runs):
        with model:
            if bound_direction == "upper":
                target.upper_bound = bound
            else:
                target.lower_bound = bound

            if add_pfba:
                add_pFBA_constraint(
                    model, biomass_reaction, objective_fraction, flux_fraction
                )
            # Redirect gurobipy output to /dev/null to avoid spamming stdout.
            with open(os.devnull, "w") as devnull:
                with contextlib.redirect_stdout(devnull):
                    sampler = cobra.sampling.OptGPSampler(
                        model=model, processes=processes, thinning=thinning
                    )
                    samples = sampler.sample(n_samples)

            valid = sampler.validate(samples) == "v"
            samples[~valid] = np.NaN
            valid_samples = samples[valid]

            # The geweke diagnostic should be considered per chain and variable, so we have
            # to reconstruct the chains from the total samples. We can calculate the
            # chain length using the same method OptGPSampler used.
            # Note: this might be different in a different version of cobrapy!
            # chain_length = np.ceil(n_samples / processes).astype(int)
            # Alternatively, we can use the fact that cobrapy returns more samples to get to
            # an equal number per process and just split the samples by the number of processes.
            for chain_index, chains in enumerate(np.split(samples, processes)):
                geweke_diagnostics[run_index * processes + chain_index, :] = geweke(
                    chains
                )
            # TODO: Check another criteria such as rhat? (Available in Arviz)

            all_samples.append(valid_samples.assign(condition=name))

    geweke_diagnostics = pd.DataFrame(
        data=geweke_diagnostics,
        columns=pd.Index(samples.columns, name="reaction"),
        index=pd.MultiIndex.from_product(
            [[i[0] for i in runs], list(range(processes))], names=["condition", "chain"]
        ),
    )

    all_samples = pd.concat(all_samples, ignore_index=True)

    # Clip small values based on tolerances
    # (This makes it easier later to filter out true inactive reactions later,
    #  it also prevents the small values from generating real but invalid correlations)
    if cutoff_tolerance:
        condition = all_samples.condition
        all_samples = all_samples.drop("condition", axis=1)
        all_samples[
            (all_samples < model.tolerance) & (all_samples > -model.tolerance)
        ] = 0
        all_samples = all_samples.assign(condition=condition)

    # TODO: Investigate?
    # Or round based on the tolerances?
    # all_samples = all_samples.round(int(-np.log10(model.tolerance)))
    # Or is is better to truncate?
    # all_samples = np.trunc((all_samples * 1e7)) * 1e-7

    if cache_dir and update_cache:
        all_samples.to_hdf(cache_samples_path, key="samples", complevel=6)
        geweke_diagnostics.to_hdf(
            cache_convergence_path, key="convergence", complevel=6
        )
    return all_samples, geweke_diagnostics


def apply_intervention(
    model, reaction, intervention, samples, bound_percentile, verbose=False
):
    # Since a change in expression doesn't modify the direction, but only the absolute
    # flux level, we also look at the absolute fluxes in the sample.
    abs_flux_limit = samples.abs().quantile(bound_percentile)
    # For overexpression, restrict to high flux level.
    if intervention == "over-expression":
        # If the reaction is only forward or backward we can keep it simple.
        if reaction.lower_bound == 0:
            reaction.lower_bound = abs_flux_limit
        elif reaction.upper_bound == 0:
            reaction.upper_bound = -abs_flux_limit
        else:
            raise NotImplementedError(
                "Over-expression intervention for reversible flux not functional yet."
            )
            # Since we want to restrict the absolute flux bounds,
            # create a helper variable and constraints.
            # TODO: This works for straightforward optimization, but not for sampling
            #       or FVA (constraints break after using of FVA...).
            #       Also it does not work if the model is not optimized before
            #       adding the constraints.
            #       Easier to split the reaction into fw + rev, but will require
            #       a change in post-processing of the sampling.
            # TODO: Does it impact the flux distribution to have two reactions
            #       that are subtracted?
            # TODO: First check if the constraints and proxy variable already exist and
            #       modify their bounds if they do instead.
            abs_proxy = model.problem.Variable(
                name="abs_proxy_{}".format(reaction.id),
                lb=10,
                ub=COBRA_SOFT_INFINITY,
                type="continuous",
            )
            c1 = model.problem.Constraint(
                expression=reaction.forward_variable
                - (abs_proxy - reaction.reverse_variable),
                name="abs_proxy_{}_c_pos".format(reaction.id),
                lb=0,
                ub=None,
            )
            c2 = model.problem.Constraint(
                expression=reaction.reverse_variable
                - (abs_proxy - reaction.forward_variable),
                name="abs_proxy_{}_c_neg".format(reaction.id),
                lb=0,
                ub=None,
            )
            model.add_cons_vars([abs_proxy, c1, c2])
        if verbose:
            print(
                "Target {} ({}) constrained to >= {}".format(
                    reaction, intervention, abs_flux_limit
                )
            )
    # For knock-down, restrict to low flux level.
    elif intervention == "knock-down":
        if reaction.reversibility:
            reaction.bounds = (-abs_flux_limit, abs_flux_limit)
        elif reaction.lower_bound == 0:
            reaction.lower_bound = -abs_flux_limit
        elif reaction.upper_bound == 0:
            reaction.upper_bound = abs_flux_limit
        if verbose:
            print(
                "Target {} ({}) constrained to <= {}".format(
                    reaction, intervention, abs_flux_limit
                )
            )
    # For knock-out, restrict to no flux.
    elif intervention == "knock-out":
        reaction.lower_bound, reaction.upper_bound = 0, 0
        if verbose:
            print(
                "Target {} ({}) constrained to == {}".format(reaction, intervention, 0)
            )
    else:
        raise ValueError("Invalid intervention")


def sample_pairs(
    model,
    target_df,
    samples,
    sampling_settings,
    bound_percentile=0.25,
    n_processes_per_sampler=None,
    n_concurrent_samplers=None,
    timeout=None,
    cache_dir=pathlib.Path("cache"),
    combined=False,
):
    """Sample possible interventions for significant flux differences between growth and production.

    :raises ValueError: On undefined intervention (not in {over-expression, knock-down, knock-out})
    :return: data frame of sampling results per intervention.
    :rtype: pd.DataFrame
    """
    # Since the preparation (warm-up) before sampling can start is rather long and
    # not parallelized, it is useful to run multiple samplings in parallel and not
    # to use many cores for each sampling as it would lead to large periods of
    # single core usage. As the number of samples increases, this becomes less
    # relevant.
    # ---
    # Check number of physical cores if possible, since hyper-threading will often be
    # slower for numerical workloads.
    try:
        import psutil
    except ImportError:
        available_cores = len(os.sched_getaffinity(0)) // 2
    else:
        available_cores = psutil.cpu_count(logical=False)
    # Always leave one core available for OS and process coordination.
    if n_processes_per_sampler is None and n_concurrent_samplers is None:
        # Between 1 to 4 cores per sampler
        n_processes_per_sampler = min(max(1, available_cores // 2), 4)
        # Fill up the available cores, rounding down.
        n_concurrent_samplers = available_cores // n_processes_per_sampler
    elif n_concurrent_samplers is None:
        n_concurrent_samplers = max(1, available_cores // n_processes_per_sampler)
    elif n_processes_per_sampler is None:
        n_processes_per_sampler = max(1, available_cores // n_concurrent_samplers)
    print(
        "Running {} parallel processes on {} cores each.".format(
            n_concurrent_samplers, n_processes_per_sampler
        )
    )
    sampling_settings["processes"] = n_processes_per_sampler

    # If we have one sampler only, run on a fake multiprocessing pool
    # using threads with less overhead and better debugging.
    original_model = model
    if n_concurrent_samplers == 1:
        import multiprocessing.dummy

        pool = multiprocessing.dummy.Pool
    else:
        pool = MultiLevelPool

    with pool(processes=n_concurrent_samplers) as pool:
        target_names, results = [], []
        for group, targets in target_df.groupby("target_group"):
            # Copy the model and redirect gurobipy output to /dev/null
            # to avoid spamming stdout.
            with open(os.devnull, "w") as devnull:
                with contextlib.redirect_stdout(devnull):
                    model = original_model.copy()

            # Apply the intervention on all targeted reactions.
            for _, target in targets.iterrows():
                # Get correct samples for the reaction.
                production_samples = samples[samples.condition == "producing"][
                    target.reaction
                ]
                try:
                    apply_intervention(
                        model,
                        model.reactions.get_by_id(target.reaction),
                        target.intervention,
                        production_samples,
                        bound_percentile,
                    )
                except NotImplementedError:
                    warnings.warn(
                        "NotImplementedError for {} / {} ({})\nSkipping target".format(
                            target.group_name, target.reaction, target.intervention
                        )
                    )

            # Sample, filter and validate in the child process.
            target_names.append(targets.iloc[0].group_name)
            results.append(
                pool.apply_async(
                    sample,
                    args=[
                        cobra.io.to_json(model),
                        sampling_settings,
                        cache_dir,
                        target_names[-1],
                    ],
                )
            )

        # Wait for pool to finish all work
        finished_results = []
        # TODO: Use convergence results
        for target, result in zip(target_names, results):
            try:
                samples, convergence = result.get(timeout=timeout)
                finished_results.append(samples.assign(target=target))
            except RuntimeError as e:
                warnings.warn("RunTimeError for {}: {}".format(target, e))
                finished_results.append(pd.DataFrame())

    # Save results
    if combined:
        return pd.concat(finished_results, ignore_index=True)
    else:
        return finished_results


def growth_tradeoff(
    model,
    target_df,
    samples,
    biomass_reaction_id,
    production_reaction_id,
    percentiles=None,
    n_processes=4,
):
    """Calculate the reaction flux - growth tradeoff for each target group."""
    if percentiles is None:
        percentiles = np.linspace(0, 1, 20)
    if isinstance(percentiles, int):
        percentiles = np.linspace(0, 1, percentiles)
    data = []
    for group, targets in target_df.groupby("target_group"):
        for percentile in percentiles:
            with model:
                # Apply the intervention on all targeted reactions.
                for _, target in targets.iterrows():
                    # Get correct samples for the reaction.
                    production_samples = samples[samples.condition == "producing"][
                        target.reaction
                    ]
                    try:
                        apply_intervention(
                            model,
                            model.reactions.get_by_id(target.reaction),
                            target.intervention,
                            production_samples,
                            percentile,
                            verbose=False,
                        )
                    except NotImplementedError:
                        warnings.warn(
                            "NotImplementedError for {} / {} ({})\nSkipping target".format(
                                target.group_name, target.reaction, target.intervention
                            )
                        )
                # Optimize for biomass and extra production possible and vice versa.
                biomass_reaction = model.reactions.get_by_id(biomass_reaction_id)
                production_reaction = model.reactions.get_by_id(production_reaction_id)
                with model:
                    model.objective = biomass_reaction
                    biomass_I = model.optimize().objective_value
                    biomass_reaction.bounds = (biomass_I, biomass_I)
                    model.objective = production_reaction
                    production_I = model.optimize().objective_value
                with model:
                    model.objective = production_reaction
                    production_II = model.optimize().objective_value
                    production_reaction.bounds = (production_II, production_II)
                    model.objective = biomass_reaction
                    biomass_II = model.optimize().objective_value
                data.append(
                    (
                        group,
                        target.group_name,
                        percentile,
                        biomass_I,
                        production_I,
                        production_II,
                        biomass_II,
                    )
                )
    return pd.DataFrame(
        data=data,
        columns=[
            "target_group",
            "group_name",
            "percentile",
            "biomass",
            "leftover_production",
            "production",
            "leftover_biomass",
        ],
    )


def score_reactions(
    model, samples, target_export_id, biomass_id, essential, gene_name=None
):
    """Score reactions based on several criteria."""
    # Check for differences in distributions.
    significance_results = []
    c1, c2, c3 = samples.condition.unique()
    for column in samples.columns:
        if column == "condition":
            continue
        c1_samples = samples[column][samples.condition == c1]
        c2_samples = samples[column][samples.condition == c2]
        c3_samples = samples[column][samples.condition == c3]
        # TODO: Check bug with 10.000 values and asymp (p=0) vs exact (p=1)
        # Not easily reproducible with random normal data...
        # use 'asymp' for now. It should be more or less valid anyway with
        # the number of samples we're using.
        ks1, pval1 = scipy.stats.ks_2samp(c1_samples, c2_samples, "two-sided", "asymp")
        ks2, pval2 = scipy.stats.ks_2samp(c1_samples, c3_samples, "two-sided", "asymp")
        ks3, pval3 = scipy.stats.ks_2samp(c2_samples, c3_samples, "two-sided", "asymp")
        significance_results.append(
            (
                column,
                c1,
                c1_samples.mean(),
                c1_samples.std(),
                c2,
                c2_samples.mean(),
                c2_samples.std(),
                c3,
                c3_samples.mean(),
                c3_samples.std(),
                ks1,
                pval1,
                ks2,
                pval2,
                ks3,
                pval3,
            )
        )
    results = pd.DataFrame(
        significance_results,
        columns=[
            "reaction",
            "condition_1",
            "mean (condition_1)",
            "sd (condition_1)",
            "condition_2",
            "mean (condition_2)",
            "sd (condition_2)",
            "condition_3",
            "mean (condition_3)",
            "sd (condition_3)",
            "ks-statistic (1v2)",
            "p-value (1v2)",
            "ks-statistic (1v3)",
            "p-value (1v3)",
            "ks-statistic (2v3)",
            "p-value (2v3)",
        ],
    ).set_index(["reaction"])

    # Do multiple testing correction
    # Not really useful since with 10k samples everything will be significant.
    # rejected, pval_corrected, alphacSidak, alphacBonf = statsmodels.stats.multitest.multipletests(
    #   results["p-value"], alpha=0.05, method="bonferroni"
    # )

    results = results.assign(
        **{
            "name": [model.reactions.get_by_id(r).name for r in results.index],
            "reaction_equation": [
                model.reactions.get_by_id(r).build_reaction_string(True)
                for r in results.index
            ],
            "gene_rule": [
                model.reactions.get_by_id(r).gene_name_reaction_rule
                for r in results.index
            ],
            "mean_fold_change": np.abs(
                results["mean (condition_3)"] / results["mean (condition_1)"]
            ),
            "mean_rel_change": np.abs(
                np.abs(
                    np.abs(results["mean (condition_3)"])
                    - np.abs(results["mean (condition_1)"])
                )
                / (
                    results[["mean (condition_3)", "mean (condition_1)"]]
                    .abs()
                    .max(axis=1)
                )
            ),
            "mean_absolute_change": np.abs(
                np.abs(
                    np.abs(results["mean (condition_3)"])
                    - np.abs(results["mean (condition_1)"])
                )
            ),
            "essential": results.index.isin(essential),
            "target_correlation": (
                samples[samples.condition == "producing"]
                .drop("condition", axis=1)
                .corrwith(samples[samples.condition == "producing"][target_export_id])
            ),
            "biomass_correlation": (
                samples[samples.condition == "producing"]
                .drop("condition", axis=1)
                .corrwith(samples[samples.condition == "producing"][biomass_id])
            ),
        }
    )
    if gene_name is not None:
        results = results.assign(
            gene_names=results.gene_rule.replace(gene_name, regex=True)
        )
    return results


def get_significant_reactions(results, significance_thresholds):
    """Select significant reactions based on thresholds."""
    ks_cutoff_1 = significance_thresholds["ks_cutoff_1"]
    ks_cutoff_2 = significance_thresholds["ks_cutoff_2"]
    p_cutoff = significance_thresholds["p_cutoff"] / len(results)
    biomass_correlation_max = significance_thresholds["biomass_correlation_max"]
    abs_change_cutoff = significance_thresholds["abs_change_cutoff"]

    significant = results[
        (results["ks-statistic (1v3)"] >= ks_cutoff_1)
        & (results["ks-statistic (2v3)"] >= ks_cutoff_2)
        & (results["p-value (1v3)"] <= p_cutoff)
        & (results["p-value (2v3)"] <= p_cutoff)
        & (results["biomass_correlation"].abs() <= biomass_correlation_max)
        & (results["mean_absolute_change"] >= abs_change_cutoff)
        & (results["gene_rule"] != "")
    ].sort_values("mean_fold_change")

    intervention = np.empty(len(significant), dtype="U15")
    intervention[significant.mean_fold_change > 1] = "over-expression"
    intervention[significant.mean_fold_change < 1] = "knock-down"
    intervention[
        (significant.mean_fold_change < 1) & ~(significant.essential)
    ] = "knock-out"
    notes = np.empty(len(significant), dtype="U100")
    notes[significant.gene_rule.str.contains("or")] = "Iso-enzymes"

    significant = significant.assign(intervention=intervention, notes=notes)
    return significant


def get_correlation_groups(
    samples,
    condition,
    significant=None,
    abs_correlation=True,
    method="average",
    metric="euclidean",
    criterion="distance",
    cut_threshhold=0.25,
):
    """Cluster reactions into groups based on correlation of the absolute fluxes between samples."""
    # Take the right condition
    df = samples.loc[samples.condition == condition]
    # Drop insignificant (if wanted)
    if significant is not None:
        df = df[significant.index]
    # Drop all 0 rows.
    df = df.loc[:, (df != 0).any(axis=0)]

    # Calculate correlations.
    correlation = df.corr()
    if abs_correlation:
        correlation = np.abs(correlation)

    # Identify correlation groups using clustering.
    linkage_matrix = scipy.cluster.hierarchy.linkage(
        correlation, method=method, metric=metric
    )
    groups = pd.Series(
        scipy.cluster.hierarchy.fcluster(
            linkage_matrix, cut_threshhold, criterion=criterion
        ),
        index=correlation.index,
    )
    return groups, correlation, linkage_matrix


def get_top_picks(
    significant, filter_iso_enyzmes=True, sort_by="mean_fold_change", ascending=True
):
    """Sort top picks per group, splitting on knock-down/out and over-expression."""
    knockout_targets = []
    overexpression_targets = []
    for group in significant.correlation_group.unique():
        selection = significant[significant.correlation_group == group]
        selection = selection[~selection.notes.str.contains("Iso-enzymes")]
        knockout_selection = selection[
            ~selection.intervention.str.contains("over-expression")
        ]
        overexpression_selection = selection[
            selection.intervention.str.contains("over-expression")
        ]
        if len(knockout_selection) > 0:
            target = knockout_selection.sort_values(sort_by, ascending=ascending).iloc[
                0
            ]
            knockout_targets.append(target)
        if len(overexpression_selection) > 0:
            target = overexpression_selection.sort_values(
                sort_by, ascending=ascending
            ).iloc[0]
            overexpression_targets.append(target)
    return pd.DataFrame(knockout_targets), pd.DataFrame(overexpression_targets)


def summarize_results(
    samples,
    model,
    target_export_id,
    biomass_reaction_id,
    essential,
    gene_name,
    significance_thresholds,
):
    nonzero_samples = samples.loc[:, ~(samples == 0).all(axis=0)]

    results = score_reactions(
        model,
        nonzero_samples,
        target_export_id,
        biomass_reaction_id,
        essential,
        gene_name,
    )

    significant = get_significant_reactions(results, significance_thresholds)
    df = results.assign(
        significant=results.index.isin(significant.index),
        intervention=significant.intervention,
    )
    df.intervention = df.intervention.fillna("")

    condition = "producing"
    groups, correlation, linkage_matrix = get_correlation_groups(
        nonzero_samples, condition, None
    )

    # Assign correlation groups
    df = df.assign(correlation_group=groups, iso_enzyme=df.gene_rule.str.contains("or"))
    df.correlation_group = df.correlation_group.fillna(0).astype(int)

    # Organize per gene
    reaction_per_gene = (
        df.gene_names.str.replace(r"[\(\)]", "", regex=True)
        .str.split(r" and | or ")
        .apply(pd.Series, 1)
        .stack()
        .reset_index()
        .drop("level_1", axis=1)
        .set_axis(["reaction", "gene"], axis=1)
        # Sometimes genes occur multiple times in the same formula
        .drop_duplicates()
    )

    results_per_gene = (
        df.loc[reaction_per_gene.reaction]
        .assign(gene=reaction_per_gene.gene.values)
        .reset_index()
    )

    # Re-organize columns
    columns = [
        "gene",
        "reaction",
        "correlation_group",
        "intervention",
        "essential",
        "iso_enzyme",
        "gene_rule",
        "mean_rel_change",
        "mean_fold_change",
        "mean_absolute_change",
        "mean (condition_1)",
        "sd (condition_1)",
        "mean (condition_3)",
        "sd (condition_3)",
        "name",
        "reaction_equation",
    ]

    mapper = {
        "mean (condition_1)": "growing (mean)",
        "sd (condition_1)": "growing (sd)",
        "mean (condition_3)": "producing (mean)",
        "sd (condition_3)": "producing (sd)",
        "correlation_group": "group",
    }

    results_output = results_per_gene[columns].rename(columns=mapper)

    results_output.gene = results_output.gene.replace("", "~unassociated")

    # Determine order (ignoring insignificant entries)
    order = (
        results_output.loc[results_output.intervention != ""]
        .groupby("gene")
        .mean_fold_change.mean()
        .reindex(results_output.gene.unique())
        .sort_values()
    )
    output = results_output.set_index(["gene", "reaction"]).loc[order.index]
    return df, output, order, correlation


def compare_to_base(base, mutant, strain, change_threshold=0.1):
    effect = []
    for reaction in base.index | mutant.index:
        try:
            base_reaction = base.loc[reaction]
        except KeyError:
            base_reaction = None
        try:
            mutant_reaction = mutant.loc[reaction]
        except KeyError:
            mutant_reaction = None

        if base_reaction is None:
            effect.append(
                (
                    reaction,
                    strain,
                    mutant_reaction["gene_names"],
                    "new",
                    "inactive",
                    mutant_reaction.intervention,
                    1.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    mutant_reaction["mean (condition_1)"],
                    mutant_reaction["sd (condition_1)"],
                    mutant_reaction["mean (condition_3)"],
                    mutant_reaction["sd (condition_3)"],
                )
            )
        elif mutant_reaction is None:
            effect.append(
                (
                    reaction,
                    strain,
                    base_reaction["gene_names"],
                    "lost",
                    base_reaction.intervention,
                    "inactive",
                    0.0,
                    base_reaction["mean (condition_1)"],
                    base_reaction["sd (condition_1)"],
                    base_reaction["mean (condition_3)"],
                    base_reaction["sd (condition_3)"],
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                )
            )
        else:
            difference = (
                mutant_reaction.mean_absolute_change
                - base_reaction.mean_absolute_change
            ) / base_reaction.mean_absolute_change

            # FIXME: Knock-down != knock-out (It is, but only because it is determined
            # the same way for the mutant and the wild-type.)
            if not base_reaction.intervention and not mutant_reaction.intervention:
                synergy = ""
            elif base_reaction.intervention != mutant_reaction.intervention:
                if not base_reaction.intervention:
                    synergy = "new"
                elif not mutant_reaction.intervention:
                    synergy = "lost"
                else:
                    synergy = "switched"
            else:
                if difference > change_threshold:
                    synergy = "increased"
                elif difference < -change_threshold:
                    synergy = "decreased"
                else:
                    synergy = "similar"
            effect.append(
                (
                    reaction,
                    strain,
                    base_reaction["gene_names"],
                    synergy,
                    base_reaction.intervention,
                    mutant_reaction.intervention,
                    difference,
                    base_reaction["mean (condition_1)"],
                    base_reaction["sd (condition_1)"],
                    base_reaction["mean (condition_3)"],
                    base_reaction["sd (condition_3)"],
                    mutant_reaction["mean (condition_1)"],
                    mutant_reaction["sd (condition_1)"],
                    mutant_reaction["mean (condition_3)"],
                    mutant_reaction["sd (condition_3)"],
                )
            )
    effect_per_reaction = pd.DataFrame(
        effect,
        columns=[
            "reaction",
            "mutant",
            "gene",
            "synergy",
            "before",
            "after",
            "effect_size",
            "WT (growing)",
            "WT (growing) [sd]",
            "WT (producing)",
            "WT (producing) [sd]",
            "mutant (growing)",
            "mutant (growing) [sd]",
            "mutant (producing)",
            "mutant (producing) [sd]",
        ],
    ).set_index("reaction")
    effect_per_reaction.gene = effect_per_reaction.gene.replace("", "~unassociated")

    # Organize per gene (#TODO: Refactor this out - same in summarize results function)
    reaction_per_gene = (
        effect_per_reaction.gene.str.replace(r"[\(\)]", "")
        .str.split(r" and | or ")
        .apply(pd.Series, 1)
        .stack()
        .reset_index()
        .drop("level_1", axis=1)
        .set_axis(["reaction", "gene"], axis=1)
        # Sometimes genes occur multiple times in the same formula
        .drop_duplicates()
    )
    effects_per_gene = (
        effect_per_reaction.loc[reaction_per_gene.reaction]
        .assign(gene=reaction_per_gene.gene.values)
        .reset_index()
        .set_index(["gene", "reaction"])
    )

    order = (
        effects_per_gene.loc[effects_per_gene.synergy != ""]
        .groupby("gene")
        .effect_size.mean()
        .reindex(effects_per_gene.index.get_level_values(0).unique())
        .sort_values(ascending=False)
    )
    effects_per_gene.loc[order.index]
    return effect_per_reaction, effects_per_gene.loc[order.index], order


def geweke(chain, beginning_fraction=0.1, end_fraction=0.5):
    """Calculate the Geweke diagnostic for chain convergence.

    Accepts a single 1-dimensional chain, or a 2-dimensional array of chains,
    with samples as rows and chains as columns.

    Note: Ignores NaN values.

    :param chain: One or more markov chains.
    :type chain: np.array like object (np.array, pd.Series, pd.Dataframe)
    :param beginning_fraction: The beginning fraction of the chain, defaults to 0.1
    :type beginning_fraction: float, optional
    :param end_fraction: The end fraction of the chain, defaults to 0.5
    :type end_fraction: float, optional
    :raises ValueError: On invalid fractions or non 1-dimensional chain.
    """
    if (
        beginning_fraction + end_fraction >= 1
        or min(beginning_fraction, end_fraction) <= 0
    ):
        raise ValueError("Invalid fraction")

    # Split the chain.
    if chain.ndim == 1:
        chain = np.atleast_2d(chain).T
    length = chain.shape[0]

    # First part.
    end_of_beginning = int(length * beginning_fraction)
    start = chain[:end_of_beginning]

    start_of_end = length - int(length * end_fraction)
    end = chain[start_of_end:]

    # Calculate the diagnostic. Silence numpy errors caused by divide by zero.
    with np.errstate(divide="ignore", invalid="ignore"):
        geweke_diagnostic = (
            np.nanmean(start, axis=0) - np.nanmean(end, axis=0)
        ) / np.sqrt(np.nanvar(start, axis=0) + np.nanvar(end, axis=0))
    # Replace +/-infinity from divide by zeros by NaN values.
    geweke_diagnostic[np.isinf(geweke_diagnostic)] = np.NaN

    # Return in less dimension as input.
    return geweke_diagnostic if geweke_diagnostic.size > 1 else geweke_diagnostic[0]


def limit_flux(model, reaction, flux, error_range, reversed=False):
    """Limit a single flux."""
    reversed = -1 if reversed else 1
    reaction = model.reactions.get_by_id(reaction)
    reaction.bounds = reversed * flux - error_range, reversed * flux + error_range


def limit_flux_sum(model, reactions, flux, error_range, reversed=None):
    """Limit the sum of flux through a group of reactions."""
    reversed = {} if reversed is None else reversed
    reversed = {
        reaction: -1 if reversed.get(reaction, False) else 1 for reaction in reactions
    }
    reactions = [model.reactions.get_by_id(reaction) for reaction in reactions]
    constraint = model.problem.Constraint(
        sum(reversed[reaction.id] * reaction.flux_expression for reaction in reactions),
        lb=flux - error_range,
        ub=flux + error_range,
    )
    model.add_cons_vars(constraint)


def add_pFBA_constraint(model, objective, objective_fraction=1, flux_fraction=1):
    """Add a pFBA flux minimality constraint to the model.

    :param model: The cobra model
    :type model: cobra.Model
    :param objective: The objective that should be maximized,
    in order to determine the objective bound for the flux sum minimization.
    :type objective: cobra.Reaction
    :param objective_fraction: pFBA minimum objective fraction, should be 1<= and defaults to 1.
    :type objective_fraction: float, optional
    :param flux_fraction: pFBA maximum flux fraction, should be >=1 and defaults to 1.
    :type flux_fraction: float, optional
    :return: The model with the flux minimality constraint added.
    :rtype: cobra.Model
    """

    def recursive_sum(x):
        """Recursive version of sum.

        Sum operations are slow on variables. By doing it recursively, we can use
        2log(length(x)) additions instead of length(x).
        We could exceed the recursion depth, but it is not very likely with normal model sizes.
        """
        length = len(x)
        if length == 1:
            return x[0]
        elif length == 2:
            return x[0] + x[1]
        else:
            split = length // 2
            return recursive_sum(x[split:]) + recursive_sum(x[:split])

    # Calculate flux sum for the objective.
    flux_sum_value = cobra.flux_analysis.pfba(model, objective_fraction, objective)

    # Create a total flux variable (fw + rev fluxes, same as pFBA does it in Cobra.)
    variables = ((r.forward_variable, r.reverse_variable) for r in model.reactions)
    sum_variable = recursive_sum(list(itertools.chain.from_iterable(variables)))
    # Create and add the constraint to the model.
    constraint = model.problem.Constraint(
        expression=sum_variable,
        name="total_flux_sum_bound",
        lb=0,
        ub=flux_sum_value.objective_value * flux_fraction,
    )
    model.add_cons_vars([constraint])


def set_medium(model, medium, mapping):
    """Set a minimal medium."""
    model.reactions.get_by_id(mapping.glucose_exchange).bounds = medium["glucose"]
    model.reactions.get_by_id(mapping.oxygen_exchange).bounds = medium["oxygen"]
    model.reactions.get_by_id(mapping.ammonia_exchange).bounds = medium["ammonia"]
    model.reactions.get_by_id(mapping.phosphate_exchange).bounds = medium["phosphate"]
    for reaction in mapping.mineral_exchanges.split(";"):
        model.reactions.get_by_id(reaction).bounds = medium["minerals"]


def convert_to_reaction_targets(target_df, model, strict=False, strict_ko=True):
    """Convert a set of target genes to the corresponding reactions."""
    gene_rules = cobra.manipulation.get_compiled_gene_reaction_rules(model)
    new_df_entries = []

    # This uses the gene reaction rules as used by cobra.
    find_targets_strict = lambda genes: cobra.manipulation.find_gene_knockout_reactions(
        model, genes, gene_rules
    )
    # This assumes that a knock-out / overexpression will always result in a flux change,
    # even if there are iso-enzymes.
    find_targets_nonstrict = lambda genes: set(
        itertools.chain.from_iterable(
            model.genes.get_by_id(gene).reactions for gene in genes
        )
    )

    find_targets = find_targets_strict if strict else find_targets_nonstrict
    find_targets_ko = find_targets_strict if strict_ko else find_targets_nonstrict

    for group, targets in target_df.groupby("target_group"):
        # Collect all targeted reactions
        genes, reactions = [], []
        # Do we have a reaction or gene as a target?
        for _, target in targets.iterrows():
            # Reactions can be added right away.
            if target.type == "reaction":
                reactions.append((target.id, target.intervention))
            # Genes have to be combined first and considered all at once.
            elif target.type == "gene":
                genes.append((target.id, target.intervention))

        # Next convert the genes to reactions
        # First consider all knock-outs.
        ko_reactions = find_targets_ko(
            gene for gene, intervention in genes if intervention == "knock-out"
        )
        for reaction in ko_reactions:
            reactions.append((reaction.id, "knock-out"))

        # Then consider all knock-downs + knock-outs for down-regulation.
        kd_reactions = find_targets(
            gene
            for gene, intervention in genes
            if intervention in ("knock-out", "knock-down")
        )
        for reaction in kd_reactions:
            # Only if the reaction isn't knocked out already!
            if reaction not in ko_reactions:
                reactions.append((reaction.id, "knock-down"))

        # Finally, all over-expressions can be considered.
        oe_reactions = find_targets(
            gene for gene, intervention in genes if intervention == "over-expression"
        )
        for reaction in oe_reactions:
            reactions.append((reaction.id, "over-expression"))

        # Add all new entries.
        for reaction, intervention in reactions:
            new_df_entries.append((group, target.group_name, reaction, intervention))
    return pd.DataFrame(
        new_df_entries,
        columns=["target_group", "group_name", "reaction", "intervention"],
    )


def add_naphth(model, mapping, target_id):
    # Create metabolite for Napththalenetetrol
    naphth_c = cobra.Metabolite(target_id)
    naphth_c.name = "Napthalenetetrol"
    naphth_c.compartment = mapping.cytosol

    # Create reaction to produce Napthalenetetrol
    naphthsyn = cobra.Reaction("naphthsyn")
    naphthsyn.name = "Napthalenetetrol synthesis"
    naphthsyn.add_metabolites(
        {
            model.metabolites.get_by_id(mapping.malonyl_coa_c): -5,
            model.metabolites.get_by_id(mapping.co2_c): 1,
            model.metabolites.get_by_id(mapping.coa_c): 5,
            model.metabolites.get_by_id(mapping.h2o_c): 5,
            naphth_c: 1,
        }
    )

    # Create an export for the target.
    target_export = cobra.Reaction("EX_naphth_export")
    target_export.name = "Export reaction for {}".format(target_id)
    target_export.add_metabolites({naphth_c: -1})
    target_export.bounds = (0, float("inf"))

    # Add the reactions and metabolites to the model.
    model.add_reactions([naphthsyn, target_export])
    return naphth_c, naphthsyn, target_export


def verify_target_production(
    model, biomass_reaction, target_export, target_id, verbose=True
):
    # Verify the model can produce the target and can grow.
    with model:
        model.objective = biomass_reaction
        b1 = model.optimize("maximize").objective_value
        assert b1 > 0
        biomass_reaction.lower_bound = b1
        model.objective = target_export
        t1 = model.optimize("maximize").objective_value
        if verbose:
            print(
                "Native maximum biomass production: {:.2f}, allowing for "
                "{:.3f} production of {}.".format(b1, t1, target_id)
            )
    with model:
        model.objective = target_export
        t2 = model.optimize("maximize").objective_value
        assert t2 > 0
        target_export.lower_bound = t2
        model.objective = biomass_reaction
        b2 = model.optimize("maximize").objective_value
        if verbose:
            print(
                "Native maximum {} production: {:.2f}, allowing for "
                "{:.3f} production of biomass.".format(target_id, t2, b2)
            )
    return b1, t1, b2, t2


def get_reaction_sets(model, mapping, ids=False):
    # All reactions as a set
    all_reactions = set(model.reactions)
    # Necessary reactions for the model to optimize properly
    required = {model.reactions.get_by_id(i) for i in (mapping.atpm, mapping.biomass)}
    # All reactions that are not biological can be ignored.
    not_biological = (
        set(model.boundary)
        | set(model.exchanges)
        | set(model.sinks)
        | set(model.demands)
    )
    # Blocked reactions that can't carry flux.
    blocked = {
        model.reactions.get_by_id(i)
        for i in cobra.flux_analysis.find_blocked_reactions(model)
    }
    # Reactions without genes.
    no_genes = {r for r in model.reactions if not r.genes}
    # essential reactions
    essential = cobra.flux_analysis.find_essential_reactions(model)
    # reactions from essential genes
    essential_genes = cobra.flux_analysis.find_essential_genes(model)
    # transport reactions
    transport = {
        r for r in model.reactions if len({i.compartment for i in r.metabolites}) > 1
    }
    results = (
        all_reactions,
        required,
        not_biological,
        blocked,
        no_genes,
        essential,
        essential_genes,
        transport,
    )
    if ids:
        return tuple({r.id for r in l} for l in results)
    else:
        return results


def gene_to_fbc_gene(gene):
    """Convert gene in the form of PP{number}+ to gp_PP_(__{number}__)+."""
    numbers = {
        "0": "ZERO",
        "1": "ONE",
        "2": "TWO",
        "3": "THREE",
        "4": "FOUR",
        "5": "FIVE",
        "6": "SIX",
        "7": "SEVEN",
        "8": "EIGHT",
        "9": "NINE",
    }
    return "".join(["gp_PP_"] + ["__{}__".format(numbers[c]) for c in gene[-4:]])


def fbc_gene_to_gene(gene):
    """Convert fbc gene in the form of gp_PP_(__{number}__)+ to PP{number}+."""
    numbers = {
        "ZERO": "0",
        "ONE": "1",
        "TWO": "2",
        "THREE": "3",
        "FOUR": "4",
        "FIVE": "5",
        "SIX": "6",
        "SEVEN": "7",
        "EIGHT": "8",
        "NINE": "9",
    }
    return "".join(["PP"] + [numbers[i] for i in gene.strip("gpP_").split("____")])
