"""Sampling code for malonyl-CoA overproduction in P. putida."""
import hashlib
import json
import pathlib
import warnings

import cobra
import numpy as np
import pandas as pd

from sampling import *


if __name__ == "__main__":
    # ----------------------------------------------------------------------
    # General settings
    # ----------------------------------------------------------------------
    # Run pairwise analysis as well (yes/np) - Warning: Can take a long time.
    pairwise = False

    data_directory = pathlib.Path("data")
    model_directory = data_directory / "models"
    # Sampling results are stored here, so they can be reused if the settings are not changed.
    cache_directory = pathlib.Path("cache")
    model_name = "iJN1462"

    mapping = pd.read_csv(model_directory / "model_mapping.csv", index_col=0)[
        model_name
    ]

    results_directory = pathlib.Path("results")
    results_directory.mkdir(parents=True, exist_ok=True)

    model_path = pathlib.Path(model_directory / mapping.file).resolve()

    target_id = "naphth_c"
    biomass_reaction_id = mapping.biomass
    substrate_id = mapping.glucose_e
    substrate_exchange_id = mapping.glucose_exchange
    atpm_reaction_id = mapping.atpm
    solver = "gurobi"
    warnings.filterwarnings("always", module="__main__")

    # ----------------------------------------------------------------------
    # Loading model & medium.
    # ----------------------------------------------------------------------
    if cache_directory:
        cache_directory.mkdir(exist_ok=True, parents=True)
    # Do not replace R_ / M_ prefixes by setting f_replace={}.
    model = cobra.io.read_sbml_model(str(model_path), f_replace={})
    biomass_reaction = model.reactions.get_by_id(biomass_reaction_id)
    atpm_reaction = model.reactions.get_by_id(atpm_reaction_id)
    substrate = model.metabolites.get_by_id(substrate_id)
    substrate_exchange = model.reactions.get_by_id(substrate_exchange_id)
    model.solver = solver
    max_tolerance = model.tolerance

    # Set medium
    inf = float("inf")
    medium = {
        "glucose": (-6.3, inf),
        "oxygen": (-inf, inf),
        "ammonia": (-inf, inf),
        "phosphate": (-inf, inf),
        "minerals": (-inf, inf),
    }

    set_medium(model, medium, mapping)
    naphth_c, naphthsyn, target_export = add_naphth(model, mapping, target_id)
    target_export_id = target_export.id

    # Constrain all reactions bounds to a maximum.
    max_bound = COBRA_SOFT_INFINITY
    for reaction in model.reactions:
        min_, max_ = reaction.bounds
        reaction.bounds = (max(min_, -max_bound), min(max_, max_bound))

    if cache_directory:
        reaction_sets_file = (
            cache_directory
            / "reaction_sets"
            / "{}.json".format(
                hashlib.sha1(
                    cobra.io.to_json(model, sort=True).encode("ascii")
                ).hexdigest()
            )
        )
        if reaction_sets_file.exists():
            warnings.warn(
                "Retrieved cached reaction sets from {}".format(str(reaction_sets_file))
            )
            reaction_sets = tuple(
                set(i) for i in json.loads(reaction_sets_file.read_text())
            )
        else:
            reaction_sets = get_reaction_sets(model, mapping, ids=True)
            reaction_sets_file.parent.mkdir(exist_ok=True)
            reaction_sets_file.write_text(
                json.dumps([list(i) for i in reaction_sets], sort_keys=True, indent=2)
            )
    else:
        reaction_sets = get_reaction_sets(model, mapping, ids=True)
    all_reactions, required, not_biological, blocked = reaction_sets[:4]
    no_genes, essential, essential_genes, transport = reaction_sets[4:]
    ignore = required | not_biological | blocked | no_genes | transport | essential

    # Load gene annotation.
    gene_df = pd.read_csv(
        data_directory / "Pseudomonas_putida_KT2440_110.tsv",
        comment="#",
        index_col=1,
        sep="\t",
    ).drop(["Sequence", "Nucleotide Sequence", "Amino Acid Sequence"], axis=1)

    gene_df.loc["PP_0897", "Name"] = "fumA"
    gene_df.loc["PP_4379", "Name"] = "fabH"
    gene_df.loc["PP_4635", "Name"] = "fabV"
    gene_df.loc["PP_0253", "Name"] = "pck"
    gene_names = gene_df.Name.dropna().to_dict()

    # ----------------------------------------------------------------------
    # Sampling and analysis
    # ----------------------------------------------------------------------
    sampling_settings = {
        "biomass_reaction": biomass_reaction.id,
        "target_export": target_export.id,
        # Number of samples generated.
        "n_samples": 10_000,
        # Optimality required for the different cases
        # i.e. production > 0.9 * max_production
        "optimality": 0.9,
        # Number of samples thrown away for each sample generated
        "thinning": 100,
        "processes": 3,
        # A constraint is added for the total sum of flux to be less then
        # x times the WT pFBA flux. This can partially avoid unrealistic fluxes
        # with large loops etc. "objective fraction" is the pFBA optimality
        # and flux fraction is a multiplier for the added constraint.
        "objective_fraction": 1,
        "flux_fraction": 1.25,
        # Set any fluxes lower then the tolerance to 0.
        "cutoff_tolerance": True,
    }
    print("Sampling for targets...")
    valid_samples, convergence = sample(
        model, sampling_settings, cache_directory, name="WT"
    )

    # Check which reactions are significantly different and have a gene associated.
    significance_thresholds = {
        # Cutoff for distribution overlap between growth & production
        "ks_cutoff_1": 0.5,
        # Cutoff for distribution overlap between growth and slow growth.
        # This is usefull to filter out fluxes that are low / high
        # just because there is low growth in the production case.
        # i.e. transport reactions for minerals
        "ks_cutoff_2": 0.25,
        # p_cutoff will be scaled by dividing by the number of results.
        "p_cutoff": 0.05,
        # Maximal correlation that a flux can have with the biomass.
        # Can be used to filter out even more reactions that are solely linked
        # to high or low growth and unrelated to production.
        "biomass_correlation_max": 0.9,
        # This is the minimal level of change in flux we choose for a reaction
        # that is feasible for intervention.
        # One could argue that this should be lower, since some reactions might have
        # extremely low fluxes even when fully utilized, such as the production of
        # a large cofactor complex. On the other hand, these reactions are likely
        # not good targets anyway.
        "abs_change_cutoff": 1e-2,
    }

    results_per_reaction, results_per_gene, order, correlation = summarize_results(
        valid_samples,
        model,
        target_export_id,
        biomass_reaction_id,
        essential,
        gene_names,
        significance_thresholds,
    )

    # ----------------------------------------------------------------------
    # Save results
    # ----------------------------------------------------------------------
    # Identify and save top picks per group.

    with pd.ExcelWriter(results_directory / "WT_predictions.xlsx") as excelwriter:
        merge = True
        results_per_gene.loc[order.index[order < 1]].to_excel(
            excelwriter, sheet_name="genes - knock-down", merge_cells=merge
        )
        # For overexpression, reverse the order.
        results_per_gene.loc[order.index[order > 1][::-1]].to_excel(
            excelwriter, sheet_name="genes - over-express", merge_cells=merge
        )
        # Sort to get unassociated at the bottom
        results_per_gene.loc[order.index[order.isna()]].sort_index().to_excel(
            excelwriter, sheet_name="genes - insignificant", merge_cells=merge
        )
        results_per_gene.to_excel(
            excelwriter, sheet_name="genes - all", merge_cells=merge
        )
        # Complete info per reaction as well
        results_per_reaction.to_excel(excelwriter, sheet_name="reactions - all")

    # ----------------------------------------------------------------------
    # Pairwise target testing
    # ----------------------------------------------------------------------
    # Select a subset of targets for testing.

    if pairwise:
        # Name: [Gene identifiers in the model]
        # + and - denote over-expression and knock-down respectively.
        lab_targets = {
            "accBD": ["PP_0559+", "PP_1996+"],
            "fabD": ["PP_1913-"],
            "fabV": ["PP_4635-"],
            "fabA": ["PP_4174-"],
            "fabF": ["PP_1916-"],
            # "fabB": ["PP_4175-"],
            # "sucAB": ["PP_4189-", "PP_4188-"],
            # "fabH": ["PP_4379-"],
            # "gntT": ["PP_3417-"],
            # "ilvH": ["PP_4679-"],
            # "cyoC": ["PP_0814-"],
            # "pycAB": ["PP_5347-", "PP_5346-"],
            # "asnB": ["PP_1750-"],
            # "leuA": ["PP_1025-"],
            # "lysC": ["PP_4473-"],
            # "nuoC": ["PP_4121-"],
            # "gnuK": ["PP_3416-"],
            # "cyoAB": ["PP_0812-", "PP_0813-"],
            # "glnA": ["PP_5046-"],
            # "fabZ": ["PP_1602-"],
            # "glcB": ["PP_0356-"],
            # "nuoA": ["PP_4119-"],
            # "proB": ["PP_0691-"],
            # "icd": ["PP_4011-"],
            # "sdhABCD": ["PP_4193-", "PP_4191-", "PP_4190-", "PP_4192-"],
            # "sucCD": ["PP_4186-", "PP_4185-"],
            # "idh": ["PP_4012-"],
            # "ppc": ["PP_1505-"],
            # "mdh": ["PP_0654-"],
            # "gntZ": ["PP_4043-"],
            # "metK": ["PP_4967-"],
            # "hldE": ["PP_4934-"],
            # "fumC-II": ["PP_1755-"],
            # "aroE": ["PP_0074-"],
            # "fumC-I": ["PP_0944-"],
            # "scpC": ["PP_0154-"],
            # "ilvE": ["PP_3511-"],
            # "mqoII": ["PP_1251-"],
            # "maeB": ["PP_5085-"],
            # "acnA": ["PP_2112-"],
            # "argA": ["PP_5185-"],
            # "tyrB": ["PP_1972-"],
            # "gltA": ["PP_4194-"],
            # "glmM": ["PP_4716-"],
            # "purA": ["PP_4889-"],
        }
        target_gene_names = [
            gene_names.get(gene.rstrip("+-"), gene.rstrip("+-"))
            for genes in lab_targets.values()
            for gene in genes
        ]
        target_df = []
        for i, (group_name, genes) in enumerate(lab_targets.items()):
            for gene in genes:
                intervention = "knock-down" if gene[-1] == "-" else "over-expression"
                target_df.append(
                    (
                        i,
                        "gene",
                        "G_{}".format(gene[:-1]),
                        intervention,
                        group_name,
                        gene_df.loc[gene[:-1]].Name,
                    )
                )
        target_df = pd.DataFrame(
            data=target_df,
            columns=[
                "target_group",
                "type",
                "id",
                "intervention",
                "group_name",
                "name",
            ],
        )

        # Convert to only reactions instead of mixed genes and reactions
        lab_target_reactions = convert_to_reaction_targets(
            target_df, model, strict=False, strict_ko=True
        )

        # Sample for each target.
        print("Sampling pairs...")
        n_processes_per_sampler = 1
        n_concurrent_samplers = 4
        # For now, we assume all knock-outs are actually implemented as knock-down.
        pairwise_samples = sample_pairs(
            model,
            lab_target_reactions,
            # Use valid samples here since reactions might be included that
            # were not active in the first place.
            valid_samples,
            sampling_settings,
            # The percentile of the absolute WT flux that we set as a new bound.
            # i.e. for a knock-down of a reversible reaction:
            # -percentile(abs(WT), 0.25) < flux < percentile(abs(WT), 0.25)
            bound_percentile=0.25,
            cache_dir=cache_directory,
            n_processes_per_sampler=n_processes_per_sampler,
            n_concurrent_samplers=n_concurrent_samplers,
        )

        # Analyse outputs per target
        print("Analysing pairs...")
        # The relative difference to the WT effect
        # if between -change_treshold and +change_threshold we will mark it as
        # similar to the reference.
        change_threshold = 0.1
        mutant_effects = {}
        mutant_results = {}
        # TODO: Parellize this loop as well?
        for mutant_samples in pairwise_samples:
            mutant = mutant_samples["target"][0]
            print("\t", mutant)
            mutant_results[mutant] = summarize_results(
                mutant_samples.drop("target", axis=1),
                model,
                target_export_id,
                biomass_reaction_id,
                essential,
                gene_names,
                significance_thresholds,
            )

            mutant_effects[mutant] = compare_to_base(
                results_per_reaction,
                mutant_results[mutant][0],
                mutant,
                change_threshold,
            )

        targets = (
            pd.concat(
                {mutant: results[2] for mutant, results in mutant_results.items()}
            )
            .unstack()
            .T
        )
        # Add WT as reference
        targets.insert(
            0,
            "WT",
            (
                results_per_gene[results_per_gene.intervention != ""]
                .groupby("gene")
                .mean_fold_change.mean()
                .reindex(results_per_gene.index.get_level_values(0).unique())
                .sort_values(ascending=False)
            ),
        )
        ko_mask = np.sign(np.log10(targets)) == -1

        synergies = (
            pd.concat(
                {mutant: effects[2] for mutant, effects in mutant_effects.items()}
            )
            .unstack()
            .T
        )

        with pd.ExcelWriter(results_directory / "pairwise_results.xlsx") as excelwriter:
            lab_target_reactions.reset_index(drop=True).drop(
                "target_group", axis=1
            ).set_index("group_name").to_excel(excelwriter, sheet_name="targets")
            targets.to_excel(excelwriter, sheet_name="target overview")
            synergies.to_excel(excelwriter, sheet_name="synergy overview")

            for mutant in mutant_effects.keys():
                mutant_results[mutant][1].to_excel(
                    excelwriter, sheet_name="{} - base".format(mutant)
                )
                mutant_effects[mutant][1].to_excel(
                    excelwriter, sheet_name="{} - dual".format(mutant)
                )

        # Make a summary sheet as well (pairs and new).
        with pd.ExcelWriter(
            results_directory / "dual_pairs_and_new.xlsx"
        ) as excelwriter:
            lab_target_reactions.reset_index(drop=True).drop(
                "target_group", axis=1
            ).set_index("group_name").to_excel(excelwriter, sheet_name="targets")
            targets.loc[targets.index.isin(target_gene_names)].to_excel(
                excelwriter, sheet_name="target overview"
            )
            synergies.loc[synergies.index.isin(target_gene_names)].to_excel(
                excelwriter, sheet_name="synergy overview"
            )

            for mutant in mutant_effects.keys():
                df = mutant_effects[mutant][1]
                new = df.groupby("gene").synergy.value_counts().unstack()["new"] >= 1
                kd = (
                    df.groupby("gene")
                    .after.value_counts()
                    .unstack()[["knock-down", "knock-out"]]
                    .fillna(0)
                    .sum(axis=1)
                    > 1
                )
                keep = df.index.get_level_values(0).isin(
                    pd.Index(target_gene_names) | (new.index[new] & kd.index[kd])
                )
                (
                    df.loc[keep].to_excel(
                        excelwriter, sheet_name="{} - dual".format(mutant)
                    )
                )

        # with pd.ExcelWriter(results_directory / "correlations.xlsx") as excelwriter:
        #     correlation.to_excel(excelwriter, sheet_name="reaction correlation")
