"""Script to clean up the iJN1462 model."""
import collections
import pathlib

import libsbml

model_dir = model_path = pathlib.Path("data/models")
model_path = model_dir / "emi14843-sup-0003-supinfos3.xml"
model_out_path = model_dir / "iJN1462_fbc.xml"

# Load model
doc = libsbml.readSBMLFromFile(str(model_path))
model = doc.getModel()

# There are a few non-standard entries in this model:
# Compartment with no name/id (remove)
for i, comp in enumerate(model.getListOfCompartments()):
    if not comp.id:
        break
    comp.setName(comp.getId())
model.getListOfCompartments().remove(i)

# Non-integer chargers (convert to int)
# When read these will register as 0,
# so I manually retrieved them from the original model.
metabolite_charge_fixes = {
    "M_4hvcoa_c": -4,
    "M_4pvcoa_c": -4,
    "M_3hvcoa_c": -4,
    "M_3kvcoa_c": -4,
    "M_4opcoa_c": -4,
    "M_pt3coa_c": -4,
}

for met in model.getListOfSpecies():
    fbc_met = met.getPlugin("fbc")
    if met.getId() in metabolite_charge_fixes:
        fbc_met.setCharge(metabolite_charge_fixes[met.getId()])
        print("Fixed charge for {}".format(met.getId()))
    # Missing compartment
    if not met.getCompartment():
        if met.getId().endswith("_e"):
            met.setCompartment("Extra_organism")
        elif met.getId().endswith("_p"):
            met.setCompartment("Periplasm")
        elif met.getId().endswith("_c"):
            met.setCompartment("Cytosol")
        print(
            "Fixed compartment for {} to {}".format(met.getId(), met.getCompartment())
        )

# Wrong SBO identifiers for reactions (switch 'SBO:0000176' - biochemical reaction)
wrong_sbo = [
    "R_HEX1", # Annotated as exchange.
    "R_AOXHEXCYCL", # Annotated as exchange (search on "EX" gone wrong?).
]
for r_id in wrong_sbo:
    model.getListOfReactions().get(r_id).setSBOTerm(176)
    print("Fixed SBO Term for {}".format(r_id))

# Merge flux bounds.
parameters = collections.defaultdict(list)
for parameter in model.getListOfParameters():
    parameters[parameter.getValue()].append(parameter.id)

# Create parameters for the zero, min and max values
inf_bound = 999999.0
zero_bound = model.createParameter()
zero_bound.setId("zero_bound")
zero_bound.setValue(0)
zero_bound.setSBOTerm(625)
zero_bound.setConstant(True)
min_bound = model.createParameter()
min_bound.setId("min_bound")
min_bound.setValue(-inf_bound)
min_bound.setSBOTerm(625)
min_bound.setConstant(True)
max_bound = model.createParameter()
max_bound.setId("max_bound")
max_bound.setValue(inf_bound)
max_bound.setSBOTerm(625)
max_bound.setConstant(True)

# Set parameter to merged version
min_keys = parameters[min(parameters.keys())]
max_keys = parameters[max(parameters.keys())]
zero_keys = parameters[0]
for reaction in model.reactions:
    fbc_reaction = reaction.getPlugin("fbc")
    # LB
    bound = fbc_reaction.getLowerFluxBound()
    if bound in zero_keys:
        fbc_reaction.setLowerFluxBound(zero_bound.id)
    elif bound in min_keys:
        fbc_reaction.setLowerFluxBound(min_bound.id)
    elif bound in max_keys:
        fbc_reaction.setLowerFluxBound(max_bound.id)
    # UB
    bound = fbc_reaction.getUpperFluxBound()
    if bound in zero_keys:
        fbc_reaction.setUpperFluxBound(zero_bound.id)
    elif bound in min_keys:
        fbc_reaction.setUpperFluxBound(min_bound.id)
    elif bound in max_keys:
        fbc_reaction.setUpperFluxBound(max_bound.id)

# Delete old parameters
for key in min_keys + max_keys + zero_keys:
    model.removeParameter(key)

# Save the new model
success = libsbml.writeSBMLToFile(doc, str(model_out_path))
if not success:
    raise ValueError("Could not save file.")
